import time

from modules import binance
from modules import utils
from bot.bot import TradingBot
from bot import strategies

def mainMenu():
  utils.clearTerminal()
  
  print('Welcome!\nWhat would you like to do?\n')
  print('[1] Start trading')
  print('[2] Set Binance API keys')
  print('[3] Set bot parameters')
  print('[0] Exit')

  while True:
    choice = input('\nChoice: ')

    if choice == '1':
      try:
        utils.loadJSON('config/binance.json')
        data = utils.loadJSON('config/bot.json')
        TradingBot(data).startTrading()
      except:
        print("Looks like you didn't set the configurations yet.")
        time.sleep(2)
        mainMenu()
    elif choice == '2':
      setBinanceAPIKeys()
    elif choice == '3':
      setBotParameters()
    elif choice == '0':
      exit(0)
    else:
      print('Invalid choice.')

def setBinanceAPIKeys():
  utils.clearTerminal()

  print('Binance API configuration\n')
  
  key    = input('API key: ')
  secret = input('API secret: ')

  utils.clearTerminal()
  
  binanceConfig = {
    'key': key,
    'secret': secret
  }

  utils.dumpJSON(binanceConfig, 'config/binance.json')

  print('\nConfiguration saved!')
  time.sleep(2)

  mainMenu()

def setBotParameters():
  utils.clearTerminal()

  botConfig = {
    'baseAssetBlacklist': [ 
      'USDT',
      'PAX',
      'TUSD',
      'USDC',
      'USDS'
    ],
    'riskManagement': {},
    'fee': .1
  }

  print('Trading configuration\n')

  botConfig['quote']      = input('Quote asset: ')
  botConfig['marketNum']  = int(input('Number of markets: '))
  botConfig['timeframes'] = input('Timeframes (i.e.: 15m, 30m, 1h): ').split(',')
  botConfig['timeframes'] = [x.strip() for x in botConfig['timeframes']]

  strategyNames = list(strategies.strategyDict.keys())
  choices       = []
  while True:
    utils.clearTerminal()
    
    print('Strategies:\n')
    for x, y in enumerate(strategyNames):
      print(f'[{x + 1}] {y}')
    print('[0] Done')

    choice = int(input('Choice: '))
    if choice == 0:
      if len(choices) == 0:
        continue
      else:
        break
    else:
      choices.append(strategyNames[choice - 1])
      strategyNames.pop(choice - 1)
  botConfig['strategies'] = choices

  utils.clearTerminal()

  print('\nRisk management\n')

  botConfig['riskManagement']['lots']            = int(input('Number of lots to split balance in (Each lot is used for each trade): '))
  botConfig['riskManagement']['maxLossPerTrade'] = float(input('Max loss per trade (percentage): '))
  
  utils.dumpJSON(botConfig, 'config/bot.json')
  print('\nConfiguration saved!')
  time.sleep(2)

  mainMenu()

mainMenu()