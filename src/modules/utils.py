import os
import json
import math
import platform

def loadJSON(path):
  with open(os.path.join(os.getcwd(), path), 'r') as f:
    return json.loads(f.read())

def dumpJSON(data, path):
  path = os.path.join(os.getcwd(), path)
  try:
    os.makedirs('/'.join(path.split('/')[:-1]))
  except:
    pass
  with open(path, 'w') as f:
    json.dump(data, f, indent=2)

def getPrecisionMinusLength(floatStr):
  floatStr = floatStr.replace('.', '')

  count = 0
  for c in floatStr:
    if c != '0': break

    count += 1

  return len(floatStr) - count

def truncate(f, n):
  return math.floor(f * 10 ** n) / 10 ** n

def formatPrice(quantity, priceFilter):
  return truncate(quantity, str(priceFilter).replace('.', '').find('1'))

def changeByPercent(number, percent):
  if number > 0:
    return number + number * percent / 100
  else:
    return number - number * percent / 100

def getPercentChange(initial, final):
  return (final - initial) / initial * 100

def calcOrderSize(lotSize, stopSize, maxTradeLoss):
  r = lotSize / (stopSize / maxTradeLoss)

  if r < lotSize:
    return r
  else:
    return lotSize

def clearTerminal():
  system = platform.system()
  if system == 'Linux' or system == 'Darwin':
    os.system('clear')
  elif system == 'Windows':
    os.system('cls')
  